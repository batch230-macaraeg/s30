// output number 1 
db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: 0, fruitsOnSale: {$sum: 1}}},
			{$project: {_id: 0}},
		]);


// find gte stock is greater than or equal to 20
db.fruits.find(
{
	stock:{$gte: 20}
}
);

// output number 2 
db.fruits.aggregate([
	{$match: {stock: {$gte: 20}}},
	{$group: {_id: "stock", enoughStock: {$sum: 1}}},
	{$project: {_id: 0}},
	]);


// output number 3 
db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}},
		{$sort: {total: 1}},
	]);

// output number 4 
db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
		{$sort: {total: 1}},
	]);

// output number 5 
db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
		{$sort: {total: 1}},
	]);

